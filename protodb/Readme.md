# ProtoDB

ProtoDB is a golang Object Mapper that maps your models (defined using protobuf) to Database entities, Relations in an RDBMS (Postgresql, MySQL, SQLite, etc.) and Documents in document databases (Mongodb, etc.) and lets you query them.

## Limitations

ProtoDB currently does not support Relations, also the only supported Database backend currently is MongoDB.

## Defining Models

All protobuf messages that have `model_name` option and ID field (type string) are able to be used as a model.

``` protobuf
import "options.proto";

message User {
    option (protodb.model_name) = "user";

    string id  = 1;

    string username = 2;
    string hashed_password = 3;
    string first_name = 4;
    string last_name = 5;
    int32  age = 6;
    bool   verified = 7;
}
```

You have to compile your model's proto file using protoc compiler. Keep in mind that you need to include the protodb path into protoc path:

``` bash
protoc -I. -I$GOPATH/src/gitlab.com/NeevRayanPublic/gotools/protodb  --go_out=plugins=grpc:$GOPATH/src model.proto
```

## Annotating protobuf models

ProtoDB uses `protobuf options` for annotating the model message with meta data that might be needed.

Annotations can be for the whole model (like model name, models indexes, etc.) or for a specific field (like field name, not null, unique and foreign key constrain, etc.).

You can find their defenitions in `options.proto` file.

### Index annotation

In order to index a filed you should use `protodb.indexes` option. It takes an Index message, which you can construct the following way:

``` protobuf
option (protodb.indexes) = {key: "first_name", key: "last_name" , unique: true};
```

As you can see and index can be on more than one filed. It's name will be automatically generated based on fields you're indexing.

## Using models in your code

Fist you have to create an instance of the db you want:

``` go
config := mongo.Config{
    Servers:          []string{"127.0.0.1"},
    Port:             27017,
    Database:         "testdb",
    Username:         "user",
    Password:         "pass",
    AuthenticationDB: "admin",
}

db, err := mongo.NewMongoProtoDB(config)
```

Inorder to use models in your code, first you need to register them using `db.RegisterModel`. It gets a pointer to an instance of the model.

``` go
db.RegisterModel(&models.Users{}, protoDB.Hooks{})
```

It uses this instance for extracting metadata that it needs, so its content is not important at all. `RegisterModel` creates needed tabels/documents.

You can insert new instances using `db.Create`:

``` go
new_instance, err :=db.Create(&models.Users{
    UserName:  "alik",
    FirstName: "albert",
    LastName:  "minkowski",
    Verified:  True,
})
```

In order to retrive data from database and edit/delete recodrs you can use `db.QueryOn`:

``` go
res, err := db.QueryOn(&models.Users{}).Is("user_name", "alik").GetOne()

err := db.QueryOn(&models.Users{}).Or(mongo.Query().Is("user_name", "john"), mongo.Query().In("age", 18, 19, 20)).DeleteAll()

res, err := db.QueryOn(&models.Users{}).Is("user_name", "alik").Set("user_name", "Hermann").Set("age", 45).Unset("verified").Update()
```

Again, the instance passed throw `QueryOn` is used only for extracting meta data, and will not be tampered with, neither its field values are important.
