package mongo

import (
	"encoding/json"

	"github.com/globalsign/mgo/bson"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	richerror "gitlab.com/NeevRayanPublic/gotools/rich-error"
)

func (s *mongoProtoDB) bsonToProto(data bson.M, message proto.Message) error {
	if _, ok := data["_id"]; ok {
		data["id"] = data["_id"].(bson.ObjectId).Hex()
		delete(data, "_id")
	}

	jsonByteArray, marshalErr := json.Marshal(data)

	if marshalErr != nil {
		return richerror.New("Failed to marshal bson message to json string").WithError(marshalErr).WithField("message", data)
	}

	jsonStr := string(jsonByteArray)

	unmarshalErr := jsonpb.UnmarshalString(jsonStr, message)

	return richerror.New("Failed to unmarshal message from json to proto.Message").WithError(unmarshalErr).WithField("json-str", jsonStr).NilIfNoError()
}

func (s *mongoProtoDB) protoToBSON(message proto.Message) (bson.M, error) {
	marshaler := jsonpb.Marshaler{
		EnumsAsInts:  false,
		EmitDefaults: false,
		OrigName:     true,
	}

	var data map[string]interface{}
	jsonStr, marshalErr := marshaler.MarshalToString(message)

	if marshalErr != nil {
		return bson.M(data), richerror.New("Failed to marshal proto message to json string").WithError(marshalErr).WithField("message", message)
	}

	unmarshalErr := json.Unmarshal([]byte(jsonStr), &data)

	if idInterface, ok := data["id"]; ok {
		if id, isString := idInterface.(string); isString {
			data["_id"] = bson.ObjectIdHex(id)
			delete(data, "id")
		}
	}

	return bson.M(data), richerror.New("Failed to unmarshal message from json to bson").WithError(unmarshalErr).WithField("json-str", jsonStr).NilIfNoError()
}
