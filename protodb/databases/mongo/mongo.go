package mongo

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/golang/protobuf/descriptor"
	"github.com/golang/protobuf/proto"
	"gitlab.com/NeevRayanPublic/gotools/protodb"
	richerror "gitlab.com/NeevRayanPublic/gotools/rich-error"
)

type Config struct {
	Servers          []string
	Port             int
	Database         string
	Username         string
	Password         string
	AuthenticationDB string
}

type mongoProtoDB struct {
	mainSession *mgo.Session
	database    string
	models      map[reflect.Type]modelMetaData
}

type modelMetaData struct {
	collectionName string
	hooks          protodb.Hooks
	indexes        []mgo.Index
}

func NewMongoProtoDB(config Config) (protodb.ProtoDB, error) {
	connectionURL := createMongoConnectionURL(config)

	session, err := mgo.Dial(connectionURL)
	if err != nil {
		return nil, richerror.New("Failed to Dial to mongoDB").WithError(err).WithField("connectionURL", connectionURL)
	}

	// session.SetSafe(&mgo.Safe{WMode: "majority"})

	service := mongoProtoDB{
		mainSession: session,
		database:    config.Database,
		models:      make(map[reflect.Type]modelMetaData),
	}

	return &service, err
}

func (s *mongoProtoDB) RegisterModel(message descriptor.Message, hooks protodb.Hooks) error {
	if !protodb.IsMessageModel(message) {
		return richerror.New("given message is not a protodb model.")
	}

	indexes := protodb.GetModelIndexes(message)

	s.models[reflect.TypeOf(message)] = modelMetaData{
		collectionName: protodb.GetModelName(message),
		hooks:          hooks,
		indexes:        convertToMongoIndexes(indexes),
	}

	return nil
}

func (s *mongoProtoDB) Create(message proto.Message) (proto.Message, error) {
	model, err := s.getModel(message)
	if err != nil {
		return nil, richerror.New("Failed to get model from message").WithError(err)
	}

	session := s.mainSession.Copy()
	collection := session.DB(s.database).C(model.collectionName)
	defer session.Close()

	bsonMessage, err := s.protoToBSON(message)
	if err != nil {
		return nil, richerror.New("Failed to convert proto message to bson").WithError(err)
	}

	change := mgo.Change{
		Update:    bsonMessage,
		Upsert:    true,
		ReturnNew: true,
	}

	var resultBson bson.M

	// We use Find->Apply notion so we'll be able to fetch the newly created object
	_, err = collection.Find(bsonMessage).Apply(change, &resultBson)
	if err != nil {
		return nil, richerror.New("Failed to create new record").WithError(err).WithField("new-bson", bsonMessage)
	}

	returnMessage := protodb.NewProtoMessage(message)

	err = s.bsonToProto(resultBson, returnMessage)

	return returnMessage, richerror.New("Failed to convert bson to proto message").WithError(err).NilIfNoError()
}

func (s *mongoProtoDB) QueryOn(message proto.Message) protodb.DBQuery {
	model, err := s.getModel(message)
	if err != nil {
		return &mongoQuery{
			query:  bson.M{},
			update: bson.M{},
		}
	}

	return &mongoQuery{
		modelMessage: message,
		model:        model,
		query:        bson.M{},
		update:       bson.M{},
		db:           s,
	}
}

func (s *mongoProtoDB) AutoMigrate() error {
	session := s.mainSession.Copy()
	defer session.Close()

	for _, model := range s.models {
		collection := session.DB(s.database).C(model.collectionName)

		for _, index := range model.indexes {

			err := collection.EnsureIndex(index)

			if err != nil {
				return richerror.New("Can not ensure index").WithError(err).WithFields(richerror.Fields{
					"key":        index.Key,
					"collection": model.collectionName,
					"database":   s.database,
				})
			}
		}
	}

	return nil
}

func (s *mongoProtoDB) Close() error {
	s.mainSession.Close()
	return nil
}

func createMongoConnectionURL(config Config) string {
	serversWithPort := make([]string, len(config.Servers))

	for i, server := range config.Servers {
		serversWithPort[i] = fmt.Sprintf("%s:%d", server, config.Port)
	}

	serverStirng := strings.Join(serversWithPort, ",")

	return fmt.Sprintf("mongodb://%s:%s@%s/%s?authSource=%s", config.Username, config.Password, serverStirng, config.Database, config.AuthenticationDB)
}

func (s *mongoProtoDB) getModel(message proto.Message) (modelMetaData, error) {
	model, ok := s.models[reflect.TypeOf(message)]

	if !ok {
		return modelMetaData{}, richerror.New("given message has not been registered as a model.")
	}

	return model, nil
}

func convertToMongoIndexes(indexes []*protodb.Index) []mgo.Index {
	res := make([]mgo.Index, len(indexes))

	for i, index := range indexes {
		res[i] = mgo.Index{
			Key:    index.Key,
			Unique: index.Unique,
		}
	}
	return res
}
