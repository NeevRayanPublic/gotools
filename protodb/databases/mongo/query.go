package mongo

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/golang/protobuf/proto"
	"gitlab.com/NeevRayanPublic/gotools/protodb"
	richerror "gitlab.com/NeevRayanPublic/gotools/rich-error"
)

type mongoQuery struct {
	modelMessage proto.Message
	query        bson.M
	update       bson.M
	model        modelMetaData
	db           *mongoProtoDB
}

func (m *mongoQuery) Is(fieldName string, value interface{}) protodb.DBQuery {
	if fieldName == "id" {
		fieldName = "_id"
		value = bson.ObjectIdHex(value.(string))
	}

	m.query[fieldName] = value
	return m
}

func (m *mongoQuery) In(fieldName string, values ...interface{}) protodb.DBQuery {
	if fieldName == "id" {
		fieldName = "_id"
		for i, value := range values {
			values[i] = bson.ObjectIdHex(value.(string))
		}
	}

	m.query[fieldName] = bson.M{"$in": values}
	return m
}

func (m *mongoQuery) And(queries ...protodb.DBQuery) protodb.DBQuery {
	m.listOperator("$and", queries...)
	return m
}

func (m *mongoQuery) Or(queries ...protodb.DBQuery) protodb.DBQuery {
	m.listOperator("$or", queries...)
	return m
}

func (m *mongoQuery) Not(query protodb.DBQuery) protodb.DBQuery {
	m.query["$not"] = query

	return m
}

func (m *mongoQuery) Set(fieldName string, value interface{}) protodb.DBQuery {
	if _, ok := m.update["$set"]; !ok {
		m.update["$set"] = bson.M{}
	}

	m.update["$set"].(bson.M)[fieldName] = value

	return m
}

func (m *mongoQuery) Unset(fieldName string) protodb.DBQuery {
	if _, ok := m.update["$unset"]; !ok {
		m.update["$unset"] = bson.M{}
	}

	m.update["$unset"].(bson.M)[fieldName] = true

	return m
}

func (m *mongoQuery) Count() (int, error) {
	if ok, err := m.canExecuteQuery(); !ok {
		return 0, richerror.New("Can't perform operation on an incomplete query").WithError(err)
	}

	session := m.db.mainSession.Copy()
	collection := session.DB(m.db.database).C(m.model.collectionName)
	defer session.Close()

	c, err := collection.Find(m.query).Count()

	return c, richerror.New("Failed to get count of the query set").WithError(err).NilIfNoError()
}

func (m *mongoQuery) GetOne() (proto.Message, error) {
	if ok, err := m.canExecuteQuery(); !ok {
		return nil, richerror.New("Can't perform operation on an incomplete query").WithError(err)
	}

	session := m.db.mainSession.Copy()
	collection := session.DB(m.db.database).C(m.model.collectionName)
	defer session.Close()

	var resultBson bson.M
	resultMessage := protodb.NewProtoMessage(m.modelMessage)

	err := collection.Find(m.query).One(&resultBson)

	if err != nil {
		switch err {
		case mgo.ErrNotFound:
			return nil, richerror.New("Object Not Found").WithError(protodb.ErrNotFound)
		case mgo.ErrCursor:
			return nil, richerror.New("MongoDB Cursor Error").WithError(err)
		default:
			return nil, richerror.New("Unknown MongoDB Error").WithError(err)
		}
	}

	err = m.db.bsonToProto(resultBson, resultMessage)

	return resultMessage, richerror.New("Failed to get item from DB").WithError(err).NilIfNoError()
}

func (m *mongoQuery) GetAll() ([]proto.Message, error) {
	if ok, err := m.canExecuteQuery(); !ok {
		return nil, richerror.New("Can't perform operation on an incomplete query").WithError(err)
	}

	session := m.db.mainSession.Copy()
	collection := session.DB(m.db.database).C(m.model.collectionName)
	defer session.Close()

	var resultsBson []bson.M

	err := collection.Find(m.query).All(&resultsBson)

	if err != nil {
		switch err {
		case mgo.ErrCursor:
			return nil, richerror.New("MongoDB Cursor Error").WithError(err)
		default:
			return nil, richerror.New("Unknown MongoDB Error").WithError(err)
		}
	}

	resultLen := len(resultsBson)
	resultMessages := protodb.NewProtoMessageSlice(m.modelMessage, resultLen)

	for i, resultBson := range resultsBson {
		err = m.db.bsonToProto(resultBson, resultMessages[i])

		if err != nil {
			return nil, richerror.New("Failed to convert bson to proto message").WithError(err)
		}
	}

	return resultMessages, nil
}

func (m *mongoQuery) DeleteOne() error {
	if ok, err := m.canExecuteQuery(); !ok {
		return richerror.New("Can't perform operation on an incomplete query").WithError(err)
	}

	session := m.db.mainSession.Copy()
	collection := session.DB(m.db.database).C(m.model.collectionName)
	defer session.Close()

	err := collection.Remove(m.query)

	return richerror.New("Failed to get count of the query set").WithError(err).NilIfNoError()
}

func (m *mongoQuery) DeleteAll() error {
	if ok, err := m.canExecuteQuery(); !ok {
		return richerror.New("Can't perform operation on an incomplete query").WithError(err)
	}

	session := m.db.mainSession.Copy()
	collection := session.DB(m.db.database).C(m.model.collectionName)
	defer session.Close()

	_, err := collection.RemoveAll(m.query)

	return richerror.New("Failed to get count of the query set").WithError(err).NilIfNoError()
}

func (m *mongoQuery) Update() (proto.Message, error) {
	if ok, err := m.canExecuteQuery(); !ok {
		return nil, richerror.New("Can't perform operation on an incomplete query").WithError(err)
	}

	session := m.db.mainSession.Copy()
	collection := session.DB(m.db.database).C(m.model.collectionName)
	defer session.Close()

	change := mgo.Change{
		Update:    m.update,
		Upsert:    false,
		ReturnNew: true,
	}

	var resultBson bson.M
	returnMessage := protodb.NewProtoMessage(m.modelMessage)

	// We use Find->Apply notion so we'll be able to fetch the newly created object
	_, err := collection.Find(m.query).Apply(change, &resultBson)
	if err != nil {
		return nil, richerror.New("Failed to update record").WithError(err)
	}

	err = m.db.bsonToProto(resultBson, returnMessage)

	return returnMessage, richerror.New("Failed to convert bson to proto message").WithError(err).NilIfNoError()
}

func (m *mongoQuery) New() protodb.DBQuery {
	return &mongoQuery{
		query:  bson.M{},
		update: bson.M{},
	}
}

func (m *mongoQuery) listOperator(operator string, queries ...protodb.DBQuery) {
	operatorQuery := make([]bson.M, len(queries))

	for i, dbQuery := range queries {
		mQuery := dbQuery.(*mongoQuery)
		operatorQuery[i] = mQuery.query
	}

	m.query[operator] = operatorQuery
}

func (m *mongoQuery) canExecuteQuery() (bool, error) {
	if m.db == nil {
		return false, richerror.New("empty db")
	}

	if m.modelMessage == nil {
		return false, richerror.New("empty model message")
	}

	return true, nil
}
