package protodb

import (
	"io"

	"github.com/golang/protobuf/descriptor"
	"github.com/golang/protobuf/proto"
)

type ProtoDB interface {
	io.Closer
	RegisterModel(descriptor.Message, Hooks) error
	Create(proto.Message) (proto.Message, error)
	QueryOn(proto.Message) DBQuery
	AutoMigrate() error
}

type BeforeCreateHook func(proto.Message) (proto.Message, error)

type Hooks struct {
	beforeCreate BeforeCreateHook
}

type DBQuery interface {
	Is(string, interface{}) DBQuery
	In(string, ...interface{}) DBQuery

	And(...DBQuery) DBQuery
	Or(...DBQuery) DBQuery
	Not(DBQuery) DBQuery

	// Unique(string) DBQuery
	// Limit(int) DBQuery
	// Offset(int) DBQuery
	// SortBy(string, sortOrder) DBQuery

	Set(string, interface{}) DBQuery
	Unset(string) DBQuery

	// Push(string, interface{}, Direction) DBQuery
	// Pop(string, Direction) DBQuery
	// AddToSet(string, interface{}) DBQuery

	Count() (int, error)

	GetOne() (proto.Message, error)
	GetAll() ([]proto.Message, error)

	DeleteOne() error
	DeleteAll() error

	Update() (proto.Message, error)

	New() DBQuery
}

type sortOrder int

const (
	Ascending  = sortOrder(1)
	Descending = sortOrder(-1)
)

type Direction int

const (
	Head = Direction(-1)
	Tail = Direction(1)
)
