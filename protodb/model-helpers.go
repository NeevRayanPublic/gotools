package protodb

import (
	"reflect"

	"github.com/golang/protobuf/descriptor"
	"github.com/golang/protobuf/proto"
)

func IsMessageModel(message descriptor.Message) bool {
	return hasIDField(message) && hasMedelNameOption(message)
}

func GetModelName(message descriptor.Message) string {
	_, md := descriptor.ForMessage(message)
	ex, _ := proto.GetExtension(md.Options, E_ModelName)

	return *ex.(*string)
}

func GetModelIndexes(message descriptor.Message) []*Index {
	_, md := descriptor.ForMessage(message)
	ex, _ := proto.GetExtension(md.Options, E_Indexes)

	if ex == nil {
		return make([]*Index, 0)
	}

	return ex.([]*Index)
}

func hasIDField(message proto.Message) bool {
	return reflect.ValueOf(message).Elem().FieldByName("Id").IsValid()
}

func hasMedelNameOption(message descriptor.Message) bool {
	_, md := descriptor.ForMessage(message)
	_, err := proto.GetExtension(md.Options, E_ModelName)

	if err != nil {
		return false
	}

	return true
}
