package protodb

import (
	"reflect"

	"github.com/golang/protobuf/proto"
)

func NewProtoMessageSlice(sampleMessage proto.Message, length int) []proto.Message {
	sampleType := reflect.TypeOf(sampleMessage)

	newSlice := reflect.MakeSlice(reflect.SliceOf(sampleType), length, length)

	result := make([]proto.Message, length)

	for i := range result {
		newSlice.Index(i).Set(reflect.New(sampleType.Elem()))
		result[i] = newSlice.Index(i).Interface().(proto.Message)
	}

	return result
}

func NewProtoMessage(sampleMessage proto.Message) proto.Message {
	newMessageValue := reflect.New(reflect.TypeOf(sampleMessage).Elem())
	return newMessageValue.Interface().(proto.Message)
}
