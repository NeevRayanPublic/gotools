package protodb

import "errors"

var (
	ErrNotFound = errors.New("Object Not Found")
)
