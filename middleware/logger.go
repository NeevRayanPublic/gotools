package middleware

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	log "github.com/sirupsen/logrus"
	"gitlab.com/NeevRayanPublic/gotools/failure"
	"gitlab.com/NeevRayanPublic/gotools/rich-error"
)

func Logger() endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			res, err := next(ctx, request)

			if failureResponse, ok := res.(failure.Failure); ok && failureResponse.Failed() != nil {
				log.Errorln(richerror.GetTraceIfExistOrErrorRepr(failureResponse.Failed()))
			}

			if err != nil {
				log.Errorln(richerror.GetTraceIfExistOrErrorRepr(err))
			}

			return res, err
		}
	}
}
