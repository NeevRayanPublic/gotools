package middleware

import (
	"context"

	"gitlab.com/NeevRayanPublic/gotools/rich-error"

	"gitlab.com/NeevRayanPublic/gotools/jwt"
	"google.golang.org/grpc/metadata"

	"github.com/go-kit/kit/endpoint"
)

type contextKey string

const JWT_CLAIMS = contextKey("jwt-claims")

func JWT(jwtHelper jwt.Helper) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			if headers, ok := metadata.FromIncomingContext(ctx); ok {
				if rawTokens, ok := headers["x-jwt"]; ok && len(rawTokens) == 1 {
					claims, err := jwtHelper.ParseAndValidateToken(rawTokens[0])

					if err != nil {
						return nil, richerror.New("Failed to parse or validate JWT")
					}

					enrichedCtx := context.WithValue(ctx, JWT_CLAIMS, claims)
					res, err := next(enrichedCtx, request)
					return res, err
				}
				return nil, richerror.New("Failed to extract JWT")
			}
			return nil, richerror.New("Failed to read request headers")
		}
	}
}
