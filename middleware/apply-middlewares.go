package middleware

import "github.com/go-kit/kit/endpoint"

func ApplyMiddlewares(endpt endpoint.Endpoint, middlewares []endpoint.Middleware) endpoint.Endpoint {
	for _, middleware := range middlewares {
		endpt = middleware(endpt)
	}

	return endpt
}
