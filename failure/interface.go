package failure

type Failure interface {
	Failed() error
}
