package jwt

import (
	"errors"

	gojwt "github.com/dgrijalva/jwt-go"
	pb "gitlab.com/NeevRayan/timche-interface/auth"
)

type ClaimExtractor func(user pb.User) gojwt.Claims

type Helper interface {
	GenerateToken(pb.User, ClaimExtractor) (string, error)
	ParseAndValidateToken(string) (Claim, error)
}

var (
	ErrNotAuthenticated = errors.New("Not Authenticated")
	ErrJWTExpired       = errors.New("Expired")
	ErrJWTInvalid       = errors.New("Invalid JWT")
)
