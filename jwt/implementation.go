package jwt

import (
	gojwt "github.com/dgrijalva/jwt-go"
	pb "gitlab.com/NeevRayan/timche-interface/auth"
	richerror "gitlab.com/NeevRayanPublic/gotools/rich-error"
)

type Config struct {
	HMACSecret string
}

type jwtHelper struct {
	hmacSecret []byte
}

func New(config Config) Helper {
	return jwtHelper{
		hmacSecret: []byte(config.HMACSecret),
	}
}

func (j jwtHelper) GenerateToken(user pb.User, claimExtractor ClaimExtractor) (string, error) {
	claims := claimExtractor(user)

	token := gojwt.NewWithClaims(gojwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString(j.hmacSecret)

	return tokenString, richerror.New("Failed to create auth token").WithError(err).NilIfNoError()
}

func (j jwtHelper) ParseAndValidateToken(tokenString string) (Claim, error) {
	token, err := gojwt.ParseWithClaims(tokenString, NewClaim(), j.KeyFunc)

	if err != nil {
		return Claim{}, richerror.New("Failed to parse token").WithError(err)
	}

	if !token.Valid {
		return Claim{}, richerror.New("not valid token")
	}

	claims, ok := token.Claims.(*Claim)

	if !ok {
		return Claim{}, richerror.New("bad token type")
	}

	return *claims, nil
}

func (j jwtHelper) KeyFunc(token *gojwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*gojwt.SigningMethodHMAC); !ok {
		return nil, richerror.New("Unexpected signing method").WithField("algorithm", token.Header["alg"])
	}

	return []byte(j.hmacSecret), nil
}
