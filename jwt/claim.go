package jwt

import (
	"time"

	gojwt "github.com/dgrijalva/jwt-go"
)

type Claim struct {
	ID       string `json:"jti,omitempty"`
	Audience string `json:"aud,omitempty"`
	Issuer   string `json:"iss,omitempty"`

	UserID   string `json:"user-id"`
	Username string `json:"sub"`

	IssuedAt  int64 `json:"iat"`
	ExpiresAt int64 `json:"exp"`
	NotBefore int64 `json:"nbf,omitempty"`

	Type string `json:"type"`

	Data map[string]interface{} `json:"data,omitempty"`

	Permissions []string `json:"permissions,omitempty"`

	CurrentTimeFunc func() time.Time `json:"-"`
}

func NewClaim() gojwt.Claims {
	return &Claim{
		CurrentTimeFunc: time.Now,
	}
}

func (c *Claim) Valid() error {
	now := c.CurrentTimeFunc().Unix()

	if c.ExpiresAt < now {
		return ErrJWTExpired
	}

	if c.IssuedAt > now {
		return ErrJWTInvalid
	}

	if c.NotBefore != 0 && c.NotBefore > now {
		return ErrJWTInvalid
	}

	if c.UserID == "" || c.Username == "" {
		return ErrJWTInvalid
	}

	return nil
}
