package mailer

import (
	"net/mail"

	"gitlab.com/NeevRayanPublic/gotools/rich-error"

	"github.com/go-gomail/gomail"
)

type Config struct {
	SMTPServer   string
	SMTPPort     int
	SMTPUsername string
	SMTPPassword string
	SenderEmail  string
	SenderName   string
}

type mailer struct {
	dialer *gomail.Dialer
	sender mail.Address
}

func New(config Config) Mailer {
	return mailer{
		dialer: gomail.NewDialer(config.SMTPServer, config.SMTPPort, config.SMTPUsername, config.SMTPPassword),
		sender: mail.Address{Address: config.SenderEmail, Name: config.SenderName},
	}
}

func (m mailer) Send(email Email) error {
	sender, err := m.dialer.Dial()
	if err != nil {
		return richerror.New("Failed to dial to SMTP server").WithError(err)
	}
	defer sender.Close()

	email.From = m.sender
	message := getMessage(email)

	err = gomail.Send(sender, message)

	return richerror.New("Failed to send email").WithError(err).NilIfNoError()
}

func getMessage(email Email) *gomail.Message {
	message := gomail.NewMessage()

	to := convertMailAddressToRFC5322String(email.TO, message)
	cc := convertMailAddressToRFC5322String(email.CC, message)
	bcc := convertMailAddressToRFC5322String(email.BCC, message)

	message.SetHeader("To", to...)
	message.SetHeader("Cc", cc...)
	message.SetHeader("Bcc", bcc...)

	message.SetAddressHeader("From", email.From.Address, email.From.Name)
	message.SetAddressHeader("ReplyTo", email.ReplyTo.Address, email.ReplyTo.Name)

	message.SetHeader("Subject", email.Subject)

	message.SetBody("text/html", "")

	message.SetHeaders(email.Headers)

	message.SetBody("text/html", email.Body)

	return message
}

func convertMailAddressToRFC5322String(addresses []mail.Address, message *gomail.Message) []string {
	rfc5322strings := make([]string, len(addresses))

	for i, address := range addresses {
		rfc5322strings[i] = message.FormatAddress(address.Address, address.Name)
	}

	return rfc5322strings
}
