package mailer

import "net/mail"

type Mailer interface {
	Send(Email) error
}

type Email struct {
	TO, CC, BCC   []mail.Address
	From, ReplyTo mail.Address
	Subject       string
	Headers       mail.Header
	Body          string
}
