package mongo

import (
	"encoding/json"

	"gitlab.com/NeevRayanPublic/gotools/rich-error"

	"github.com/globalsign/mgo/bson"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
)

// NewBasicProtoToBsonConvertor creates a basic ProtoToBsonConvertor using JSON as mediator
func NewBasicProtoToBsonConvertor(idJSONFieldName string) ProtoToBsonConvertor {
	marshaler := jsonpb.Marshaler{
		EnumsAsInts:  false,
		EmitDefaults: false,
		OrigName:     true,
	}

	return func(pb proto.Message) (bson.M, error) {
		var data map[string]interface{}
		jsonStr, marshalErr := marshaler.MarshalToString(pb)

		if marshalErr != nil {
			return bson.M(data), richerror.New("Failed to marshal proto message to json string").WithError(marshalErr).WithField("message", pb)
		}

		unmarshalErr := json.Unmarshal([]byte(jsonStr), &data)

		if idInterface, ok := data[idJSONFieldName]; ok {
			if id, isString := idInterface.(string); isString {
				data["_id"] = bson.ObjectIdHex(id)
				delete(data, idJSONFieldName)
			}
		}

		return bson.M(data), richerror.New("Failed to unmarshal message from json to bson").WithError(unmarshalErr).WithField("json-str", jsonStr).NilIfNoError()
	}
}

// NewBasicBsonToProtoConvertor creates a basic BsonToProtoConvertor using JSON as mediator
func NewBasicBsonToProtoConvertor(idJSONFieldName string) BsonToProtoConvertor {
	return func(data bson.M, pb proto.Message) error {
		if _, ok := data["_id"]; ok {
			data[idJSONFieldName] = data["_id"].(bson.ObjectId).Hex()
			delete(data, "_id")
		}

		jsonByteArray, marshalErr := json.Marshal(data)

		if marshalErr != nil {
			return richerror.New("Failed to marshal bson message to json string").WithError(marshalErr).WithField("message", data)
		}

		jsonStr := string(jsonByteArray)

		unmarshalErr := jsonpb.UnmarshalString(jsonStr, pb)

		return richerror.New("Failed to unmarshal message from json to proto.Message").WithError(unmarshalErr).WithField("json-str", jsonStr).NilIfNoError()
	}
}
