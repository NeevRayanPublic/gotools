package mongo

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/NeevRayanPublic/gotools/database"
	"gitlab.com/NeevRayanPublic/gotools/rich-error"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"github.com/golang/protobuf/proto"
)

// NewProtoDB creates a new protobuf based MongoDB
func NewProtoDB(config Config, bsonToProtoConvertor BsonToProtoConvertor, indexes []Index) (*service, error) {
	connectionURL := createMongoConnectionURL(config)

	session, err := mgo.Dial(connectionURL)

	if err != nil {
		return nil, richerror.New("Failed to Dial to mongoDB").WithError(err).WithField("connectionURL", connectionURL)
	}

	session.SetSafe(&mgo.Safe{WMode: "majority"})

	service := service{
		mainSession:          session,
		database:             config.Database,
		collection:           config.Collection,
		bsonToProtoConvertor: bsonToProtoConvertor,
		indexes:              indexes,
	}

	err = service.ensureCollection()

	return &service, err
}

type service struct {
	mainSession          *mgo.Session
	database             string
	collection           string
	bsonToProtoConvertor BsonToProtoConvertor
	indexes              []Index

	createConvertor  ProtoToBsonConvertor
	replaceConvertor ProtoToBsonConvertor

	getQueryGenerator     QueryGenerator
	replaceQueryGenerator QueryGenerator
	listQueryGenerator    QueryGenerator

	listComplexQueryGenrator ComplexQueryGenrator
}

func (s *service) Create(newMessage proto.Message) (proto.Message, error) {
	// TODO: check for not already exising?
	if s.createConvertor == nil {
		return nil, richerror.New("No Create Convertor")
	}

	session := s.mainSession.Copy()
	defer session.Close()

	collection := session.DB(s.database).C(s.collection)

	bsonMessage, err := s.createConvertor(newMessage)

	if err != nil {
		return nil, richerror.New("Failed to convert proto message to bson").WithError(err)
	}

	change := mgo.Change{
		Update:    bsonMessage,
		Upsert:    true,
		ReturnNew: true,
	}

	var resultBson bson.M

	// We use Find->Apply notion so we'll be able to fetch the newly created object
	_, err = collection.Find(bsonMessage).Apply(change, &resultBson)

	if err != nil {
		return nil, richerror.New("Failed to create new record").WithError(err).WithField("new-bson", bsonMessage)
	}

	returnMessage := newProtoMessage(newMessage)

	err = s.bsonToProtoConvertor(resultBson, returnMessage)

	return returnMessage, richerror.New("Failed to convert bson to proto message").WithError(err).NilIfNoError()
}

func (s *service) Replace(findMessage proto.Message, queryHint interface{}, updateMessage proto.Message) (proto.Message, error) {
	if s.replaceQueryGenerator == nil {
		return nil, richerror.New("No Replace Query Generator")
	}

	if s.replaceConvertor == nil {
		return nil, richerror.New("No Replace Convertor")
	}

	session := s.mainSession.Copy()
	defer session.Close()

	collection := session.DB(s.database).C(s.collection)

	findBson, err := s.replaceQueryGenerator(findMessage, queryHint)

	if err != nil {
		return nil, richerror.New("Failed to generate query").WithError(err).WithField("find-message", findMessage)
	}

	updateBson, err := s.replaceConvertor(updateMessage)

	if err != nil {
		return nil, richerror.New("Failed to convert proto message to bson").WithError(err).WithField("update-message", updateMessage)
	}

	change := mgo.Change{
		Update:    updateBson,
		Upsert:    false,
		ReturnNew: true,
	}

	var resultBson bson.M

	_, err = collection.Find(findBson).Apply(change, &resultBson)

	if err != nil {
		return nil, richerror.New("Failed to replace message").WithError(err).WithField("find-message", findMessage).WithField("update-message", updateMessage)
	}

	returnMessage := newProtoMessage(updateMessage)

	err = s.bsonToProtoConvertor(resultBson, returnMessage)

	return returnMessage, richerror.New("Failed to convert bson to proto message").WithError(err).NilIfNoError()
}

func (s *service) Get(findMessage proto.Message, queryHint interface{}) (proto.Message, error) {
	if s.getQueryGenerator == nil {
		return nil, richerror.New("No Get Query Generator")
	}

	session := s.mainSession.Copy()
	defer session.Close()

	collection := session.DB(s.database).C(s.collection)

	findBson, err := s.getQueryGenerator(findMessage, queryHint)

	if err != nil {
		return nil, richerror.New("Failed to generate query").WithError(err).WithField("find-message", findMessage)
	}

	var resultBson bson.M

	err = collection.Find(findBson).One(&resultBson)

	if err != nil {
		switch err {
		case mgo.ErrNotFound:
			return nil, richerror.New("Object Not Found").WithError(database.ErrNotFound)
		case mgo.ErrCursor:
			return nil, richerror.New("MongoDB Cursor Error").WithError(err).WithField("find-message", findMessage)
		default:
			return nil, richerror.New("Unknown MongoDB Error").WithError(err).WithField("find-message", findMessage)
		}
	}

	returnMessage := newProtoMessage(findMessage)

	err = s.bsonToProtoConvertor(resultBson, returnMessage)

	return returnMessage, richerror.New("Failed to convert bson to proto message").WithError(err).NilIfNoError()
}

func (s *service) List(findMessage proto.Message, queryHint interface{}) ([]proto.Message, error) {
	if s.listQueryGenerator == nil {
		return nil, richerror.New("No List Query Generator")
	}

	session := s.mainSession.Copy()
	defer session.Close()

	collection := session.DB(s.database).C(s.collection)

	findBson, err := s.listQueryGenerator(findMessage, queryHint)

	if err != nil {
		return nil, richerror.New("Failed to generate query").WithError(err).WithField("find-message", findMessage)
	}

	var resultsBson []bson.M

	err = collection.Find(findBson).All(&resultsBson)

	if err != nil {
		switch err {
		case mgo.ErrCursor:
			return nil, richerror.New("MongoDB Cursor Error").WithError(err).WithField("bson-query", findBson)
		default:
			return nil, richerror.New("Unknown MongoDB Error").WithError(err).WithField("bson-query", findBson)
		}
	}

	resultLen := len(resultsBson)
	resultMessages := newProtoSlice(findMessage, resultLen)

	for i, resultBson := range resultsBson {
		err = s.bsonToProtoConvertor(resultBson, resultMessages[i])

		if err != nil {
			return nil, richerror.New("Failed to convert bson to proto message").WithError(err)
		}
	}

	return resultMessages, nil
}

func (s *service) ComplexList(queryData interface{}, queryHint interface{}, resultMessageStruct proto.Message) ([]proto.Message, error) {
	if s.listComplexQueryGenrator == nil {
		return nil, richerror.New("No List Complex Query Generator")
	}

	session := s.mainSession.Copy()
	defer session.Close()

	collection := session.DB(s.database).C(s.collection)

	findBson, err := s.listComplexQueryGenrator(queryData, queryHint)

	if err != nil {
		return nil, richerror.New("Failed to generate query").WithError(err).WithField("query-data", queryData)
	}

	var resultsBson []bson.M

	err = collection.Find(findBson).All(&resultsBson)

	if err != nil {
		switch err {
		case mgo.ErrCursor:
			return nil, richerror.New("MongoDB Cursor Error").WithError(err).WithField("bson-query", findBson)
		default:
			return nil, richerror.New("Unknown MongoDB Error").WithError(err).WithField("bson-query", findBson)
		}
	}

	resultLen := len(resultsBson)
	resultMessages := newProtoSlice(resultMessageStruct, resultLen)

	for i, resultBson := range resultsBson {
		err = s.bsonToProtoConvertor(resultBson, resultMessages[i])

		if err != nil {
			return nil, richerror.New("Failed to convert bson to proto message").WithError(err)
		}
	}

	return resultMessages, nil
}

func (s *service) Close() error {
	s.mainSession.Close()
	return nil
}

func (s *service) WithCreateConvertor(createConvertor ProtoToBsonConvertor) *service {
	s.createConvertor = createConvertor
	return s
}

func (s *service) WithReplaceConvertor(replaceConvertor ProtoToBsonConvertor) *service {
	s.replaceConvertor = replaceConvertor
	return s
}

func (s *service) WithGetQueryGenerator(getQueryGenerator QueryGenerator) *service {
	s.getQueryGenerator = getQueryGenerator
	return s
}

func (s *service) WithReplaceQueryGenerator(replaceQueryGenerator QueryGenerator) *service {
	s.replaceQueryGenerator = replaceQueryGenerator
	return s
}

func (s *service) WithListQueryGenerator(listQueryGenerator QueryGenerator) *service {
	s.listQueryGenerator = listQueryGenerator
	return s
}

func (s *service) WithListComplexQueryGenrator(listComplexQueryGenrator ComplexQueryGenrator) *service {
	s.listComplexQueryGenrator = listComplexQueryGenrator
	return s
}

func (s *service) ensureCollection() error {
	session := s.mainSession.Copy()
	defer session.Close()

	collection := session.DB(s.database).C(s.collection)

	for _, index := range s.indexes {

		err := collection.EnsureIndex(index.Index())

		if err != nil {
			return richerror.New("Can not ensure index").WithError(err).WithFields(richerror.Fields{
				"key":        index.Index().Key,
				"collection": s.collection,
				"database":   s.database,
			})
		}
	}
	return nil
}

func newProtoMessage(sampleMessage proto.Message) proto.Message {
	newMessageValue := reflect.New(reflect.TypeOf(sampleMessage).Elem())
	return newMessageValue.Interface().(proto.Message)
}

func newProtoSlice(sampleMessage proto.Message, length int) []proto.Message {
	sampleType := reflect.TypeOf(sampleMessage)

	newSlice := reflect.MakeSlice(reflect.SliceOf(sampleType), length, length)

	result := make([]proto.Message, length)

	for i := range result {
		newSlice.Index(i).Set(reflect.New(sampleType.Elem()))
		result[i] = newSlice.Index(i).Interface().(proto.Message)
	}

	return result
}

func createMongoConnectionURL(config Config) string {
	serversWithPort := make([]string, len(config.Servers))

	for i, server := range config.Servers {
		serversWithPort[i] = fmt.Sprintf("%s:%d", server, config.Port)
	}

	serverStirng := strings.Join(serversWithPort, ",")

	return fmt.Sprintf("mongodb://%s:%s@%s/%s?authSource=%s", config.Username, config.Password, serverStirng, config.Database, config.AuthenticationDB)
}
