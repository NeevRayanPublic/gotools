package mongo

import "github.com/globalsign/mgo"

type index struct {
	index mgo.Index
}

// NewIndex creates a index with default parameters for given field names
func NewIndex(fieldNames ...string) Index {
	return &index{
		index: mgo.Index{
			Key: fieldNames,
		},
	}
}

func (m *index) Unique() Index {
	m.index.Unique = true
	return m
}

func (m *index) DropDuplicate() Index {
	m.index.DropDups = true
	return m
}

func (m *index) BackgroundIndex() Index {
	m.index.Background = true
	return m
}

func (m *index) SparseIndex() Index {
	m.index.Sparse = true
	return m
}

func (m *index) Index() mgo.Index {
	return m.index
}
