package mongo

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/golang/protobuf/proto"
)

// Config contains your MongoDB configuration
type Config struct {
	Servers          []string
	Port             int
	Database         string
	Username         string
	Password         string
	Collection       string
	AuthenticationDB string
}

// Index contains index configuration
type Index interface {
	Unique() Index
	DropDuplicate() Index
	BackgroundIndex() Index
	SparseIndex() Index
	Index() mgo.Index
}

// BsonToProtoConvertor is used for converting BSON messages to Protobuf messages
type BsonToProtoConvertor func(bson.M, proto.Message) error

// ProtoToBsonConvertor is used for converting Protobuf messages to BSON messages
type ProtoToBsonConvertor func(proto.Message) (bson.M, error)

// QueryGenerator is used for converting Protobuf messages to MongoDB queries
// It takes an extra input so you'll be able to controll it's behavior using it
type QueryGenerator func(proto.Message, interface{}) (bson.M, error)

// ComplexQueryGenrator is used for creating complex (search like) queris
// It takes an extra input so you'll be able to controll it's behavior using it
type ComplexQueryGenrator func(interface{}, interface{}) (bson.M, error)
