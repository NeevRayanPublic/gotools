package protodb

import (
	"io"

	"github.com/golang/protobuf/proto"
)

type Service interface {
	io.Closer

	Create(newMessage proto.Message) (proto.Message, error)
	Replace(findMessage proto.Message, queryHint interface{}, updateMessage proto.Message) (proto.Message, error)
	Get(findMessage proto.Message, queryHint interface{}) (proto.Message, error)
	List(findMessage proto.Message, queryHint interface{}) ([]proto.Message, error)
	ComplexList(queryData interface{}, queryHint interface{}, resultMessageStruct proto.Message) ([]proto.Message, error)
}
