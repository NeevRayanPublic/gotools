package richerror

// RichError is a richer type of error that holds runtime information with itself
type RichError interface {
	error
	LowerError() error
	LineNumber() int
	FileName() string
	FunctionName() string
	Fields() map[string]interface{}
	Type() errorType
	StackTrace() []string
}

type Fields map[string]interface{}
