package richerror

import (
	"fmt"
	"strings"
)

// IndentedSprint output an indented verstion of the input array using 2 spaces indention
func IndentedSprint(lines []string) string {
	result := ""
	indent := "  "

	for i, line := range lines {
		prefixString := strings.Repeat(indent, i)
		result = result + fmt.Sprintln(prefixString, line)
	}

	return result
}

func GetTraceIfExistOrErrorRepr(err error) string {
	rich, isRich := err.(RichError)

	if isRich {
		return IndentedSprint(rich.StackTrace())
	}

	return err.Error()
}
