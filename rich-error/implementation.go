package richerror

import (
	"fmt"
	"runtime"
)

type richError struct {
	lowerError   error
	err          string
	lineNumber   int
	fileName     string
	functionName string
	fields       map[string]interface{}
	errType      errorType
}

func (r *richError) Error() string {
	return r.err
}

func (r *richError) LowerError() error {
	if re, ok := r.lowerError.(RichError); ok {
		return re.LowerError()
	}

	return r.lowerError
}

func (r *richError) LineNumber() int {
	return r.lineNumber
}

func (r *richError) FileName() string {
	return r.fileName
}

func (r *richError) FunctionName() string {
	return r.functionName
}

func (r *richError) Fields() map[string]interface{} {
	return r.fields
}

func (r *richError) Type() errorType {
	return r.errType
}

func (r *richError) StackTrace() []string {
	stackTrace := make([]string, 0)

	var currentError RichError
	var castOk bool

	currentError = r

	for true {
		currentTrace := getTrace(currentError)

		stackTrace = append(stackTrace, currentTrace)

		lowerError := currentError.LowerError()
		currentError, castOk = lowerError.(RichError)

		if !castOk {
			if lowerError == nil {
				break
			}

			lastTrace := lowerError.Error()
			stackTrace = append(stackTrace, lastTrace)
			break
		}
	}

	return stackTrace
}

func getTrace(err RichError) string {
	trace := getBaseTrace(err)

	if len(err.Fields()) != 0 {
		fields := getFieldsRepr(err.Fields())

		trace = trace + fields
	}

	return trace

}

func getBaseTrace(err RichError) string {
	return fmt.Sprintf("In %s:%s line %d: %s.",
		err.FileName(),
		err.FunctionName(),
		err.LineNumber(),
		err.Error())
}

func getFieldsRepr(fields Fields) string {
	result := "fileds:"
	for key, value := range fields {
		result = fmt.Sprintf("%s '%s'->'%v'", result, key, value)
	}

	return result
}

// -------------------------------------------------------

// WithFields appends given fileds to already existing ones
func (r *richError) WithFields(fields Fields) *richError {
	for key, value := range fields {
		r.fields[key] = value
	}
	return r
}

func (r *richError) WithField(key string, value interface{}) *richError {
	r.fields[key] = value
	return r
}

func (r *richError) WithError(lowerError error) *richError {
	r.lowerError = lowerError

	if re, ok := lowerError.(RichError); ok && r.errType == Unknown {
		r.errType = re.Type()
	}

	return r
}

func (r *richError) WithType(errType errorType) *richError {
	r.errType = errType
	return r
}

func (r *richError) NilIfNoError() RichError {
	if r.LowerError() == nil {
		return nil
	}
	return r
}

func New(err string) *richError {
	pc, file, line, _ := runtime.Caller(1)

	funcPt := runtime.FuncForPC(pc)
	function := "Unknown"
	if funcPt != nil {
		function = funcPt.Name()
	}

	return &richError{
		err:          err,
		lineNumber:   line,
		fileName:     file,
		functionName: function,
		fields:       make(map[string]interface{}, 0),
		errType:      Unknown,
	}
}
