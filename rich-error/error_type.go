package richerror

type errorType int

const (
	Unknown errorType = iota
	BadRequest
	Unauthorized
	Forbidden
	NotFound
	NotAvailable
	Timeout
	TooManyRequests
	InternalServerError
	ExternalError
)
