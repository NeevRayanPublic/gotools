package templatesmanager

import (
	"bytes"
	"io/ioutil"
	"text/template"
)

type templatesManager struct {
	templates   map[string]*template.Template
	fatalOutput string
}

// RenderTemplateWithDataAndHandleError renders the given templateName using given data,
// In case of no error during rendering it will return `rendered string, givenError`
// In case of error it will return `fatalOutput, rederingError`
func (s *templatesManager) RenderTemplateWithDataAndHandleError(templateName string, data interface{}, givenError error) (string, error) {
	response, err := renderTemplateWithData(s.templates[templateName], data)

	if err != nil {
		return s.fatalOutput, err
	}

	return response, givenError
}

func renderTemplateWithData(temp *template.Template, data interface{}) (string, error) {
	var resultBuffer bytes.Buffer

	if err := temp.Execute(&resultBuffer, data); err != nil {
		return "", err
	}

	return resultBuffer.String(), nil
}

// NewFromStrings creates a new TemplateManager using the "template name -> template string" mapping
//fatalOutput is the result it gives in case of critical failure.
func NewFromStrings(templateStrings map[string]string, fatalOutput string) (TemplatesManager, error) {
	templates := map[string]*template.Template{}

	for templateName, templateString := range templateStrings {

		temp, tempErr := template.New(templateName).Parse(templateString)

		if tempErr != nil {
			return nil, tempErr
		}

		templates[templateName] = temp
	}

	return &templatesManager{templates: templates, fatalOutput: fatalOutput}, nil
}

// NewFromFiles creates a new TemplateManager using the "template name -> template file address" mapping
//fatalOutput is the result it gives in case of critical failure.
func NewFromFiles(templateFiles map[string]string, fatalOutput string) (TemplatesManager, error) {
	templates := map[string]*template.Template{}

	for templateName, filePath := range templateFiles {

		temp, tempErr := loadTemplateFromFileAddress(templateName, filePath)
		if tempErr != nil {
			return nil, tempErr
		}

		templates[templateName] = temp
	}

	return &templatesManager{templates: templates, fatalOutput: fatalOutput}, nil
}

func loadTemplateFromFileAddress(templateName string, filePath string) (*template.Template, error) {
	content, fileErr := ioutil.ReadFile(filePath)
	if fileErr != nil {
		return nil, fileErr
	}

	templateContent := string(content)

	temp, tempErr := template.New(templateName).Parse(templateContent)

	return temp, tempErr
}
