package templatesmanager

type TemplatesManager interface {
	RenderTemplateWithDataAndHandleError(templateName string, data interface{}, givenError error) (string, error)
}
