package logger

import (
	"fmt"

	"github.com/sirupsen/logrus"
)

// Config is responsible for holding logrus's configuration
type Config struct {
	Level string
}

// ConfigLogger configures logrus, it should be called during application initialization.
func ConfigLogger(logConfig Config) error {
	if logConfig.Level != "" {
		level, err := logrus.ParseLevel(logConfig.Level)

		if err != nil {
			return fmt.Errorf("An error accured during parsing Log Level.\t error: %s\t log level: %s", err.Error(), logConfig.Level)
		}

		logrus.SetLevel(level)
	}

	return nil
}
